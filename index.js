// let posts = []; //container for out post, created post will be sent here.
// let count = 1;


//NOTE 4============================
//INTRO to fetch, learning GET REQUEST with FETCH.
//fetch() in nodeJS this is equal to server(), but here it is for front-end.
//the URL below is just an example from 'jsonplaceholder website'
fetch('https://jsonplaceholder.typicode.com/posts') //this https// is the 'endpoint' where we will communicate or make REQUESTS to an API in the parentheses.
.then((response) => response.json()) //to get the response 'data' from the 'endpoint' above
.then(data => {
	showPosts(data) //we used showPosts() again
	console.log(data)	
})
//NOTE 4===========================


//first to fetch, retrieving the form.
let addForm = document.querySelector('#form-add-post'); //# because we are using a 'queryselector'

//add an event and event listener 
addForm.addEventListener("submit", (e) => {   //'submit' is event, 'addEventListener' is the event listener.
	// console.log("hello")
	e.preventDefault()	//preventDefault() stops the form from reloading the page and resetting our JS code.
                     //we will add this almost 100% of the time to all our projects.

//({}) we will push an object that's why it is inside {}
	posts.push({
			id: count,
			title: document.querySelector("#txt-title").value, 
			body: document.querySelector("#txt-body").value
		})
	count++;
	console.log(posts);  	//display on console. //open an HTML browser, open the console and try to type/enter Title and Body    
	showPosts(posts); 		//we invoked or call const showPosts here, javascript will write it for us 
}) 			//showPosts came after we typed in line number 38				

const showPosts = (posts) => { 
	let postEntries = "";
	//loop through each post in our posts array, when you type in the title and body.
	posts.forEach((post) => {
		//+= to add what we type to the empty ""; let postEntries = "";
		postEntries += `					
		<div id = "post-${post.id}"">
			<h3 id = "post-title-${post.id}">${post.title}</h3>
			<p id = "post-body-${post.id}">${post.body}</p>
			<button onClick = "editPost(${post.id})">Edit</button>
			<button onClick = "deletePost(${post.id})">Delete</button>
		</div>
		`	//end of back ticks ``; use only back ticks., id on ${post.id} means also print the "id" attribute.
			//onClick means when clicked it will call/invoke the "editPost() function" related to 43, we will pass the "id" w/c you will see on console when you post/create
	})	
	//go to HTML and we copied the inner property "div-post-entries"
	document.querySelector("#div-post-entries").innerHTML = postEntries;
	//type anything in the forms "title" and "body" and check the console.
}
//To get the title and body of the post already exists	
const editPost = (id) => {	//get the 'title' and 'body' with the 'id' passed to the "editPost() function" 33/36
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	//populate the edit form fields **************
	document.querySelector(`#txt-edit-id`).value = id;
	document.querySelector(`#txt-edit-title`).value = title;
	document.querySelector(`#txt-edit-body`).value = body;
}
//---------------03Notes.js UPDATING A POST let us use a FOR LOOP
//on HTML line 17 we change the type 'text' to 'number' since we will be using "id" here that is in number.
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault()	//preventDefault() stops the form from reloading the page and resetting our JS code.
	// console.log(typeof document.querySelector(`#txt-edit-id`)).value
	for(let i = 0; i < posts.length; i++) {
		//use a for loop to match the 'id' of the post to be edited with the post inside of our posts array ****************
		if(document.querySelector("#txt-edit-id").value === posts[i].id.toString()) { //i = 0, so id iteration will start with 0 and will increment til' may mahanap na ID number sa console,
			//reassign the title and body of the post in the array to the new title and body
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			//show/invoke showPosts(posts) fx again to update the output
			showPosts(posts)

			//
			break;
		}
	}
})

						/////   ACTIVITY   /////
//Now type something on "title" and "body" click create, click edit and try to update.
///ACTIVITY ADDING DELETE
const deletePost = (id) => {
	let toDelete = document.getElementById(`post-${id}`)
	toDelete.remove();
}

///Sir JINO's answer
const deletePost =(id) => {
	document.querySelector(`post-${id}`).remove()
}

